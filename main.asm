[map all map-kernel-main.map]

ORG 0x00011000
BITS 64

segment .text
	; Jump to here
	call clear_regs
	mov rsi, WELCOME
	call print_hex

	sti

system_hang:
	hlt ; Wait until interrupt
	sti
	call hex_editor_interrupt ; Ideally, this is set as a dynamic inpterrupt callback
	jmp system_hang

	%include "funcs/ahci/all.asm" ; AHCI functions

	%include "funcs/base-funcs.asm" ; Should probably be a library somewhere

	%include "ISRs/defaults.asm" ; Default interrupts handlers

	%include "ISRs/0x21.asm" ; Keyboard interrupt handler

	%include "hex-editor.asm" ; Eventually this should be its own repo

segment .text

default_isr:
	; Default interrupt handler
	cli
	iretq


; 0x00 = None
; 0x01 = Key press
; 0x02 = Key release
segment .data
	MEMORY_TREE_ROOT dq 0x0000000000020000

	WELCOME db "OS has started. Press h to load the hex editor.", 0

segment .bss
	EVENT resb 1
