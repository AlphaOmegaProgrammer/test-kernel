[map all map-kernel-init.map]

ORG 0x00078200
USE64

segment .text
	; Do some misc stuff
	xor ax, ax
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax

	mov rsp, 0x00070000
	mov rbp, rsp

	; Disable the cursor
	mov dx, 0x03D4
	mov al, 0xA
	out dx, al

	inc dx
	mov al, 0x20
	out dx, al

	; Handle ACPI tables here once I figure out something to do with them



	; Parse e820
	mov rdi, 0x89ABCDEF
set_memory_tree_root_high: ; DO NOT REMOVE
	shl rdi, 32
	mov rdi, 0x01234567
set_memory_tree_root_low: ; DO NOT REMOVE

	mov rsi, 0x0007E000
parse_e820:
	add rsi, 0x10
	lodsb
	add rsi, 0x0F

	cmp al, 0
	jne parse_e820_done

	cmp al, 1
	jne parse_e820

	sub rsi, 0x20
	movsq
	movsq
	add rdi, 8
	mov qword [rdi-8], rdi
	add rsi, 8

	jmp parse_e820
parse_e820_done:



	; Set up interrupts

	; Init PICs
	mov al, 0x11
	out 0x20, al
	out 0xA0, al

	; Remap base IRQs to 32 and 40
	mov al, 0x20
	out 0x21, al
	mov al, 0x28
	out 0xA1, al

	; Set up cascasing
	mov al, 0x04
	out 0x21, al
	mov al, 0x02
	out 0xA1, al

	; Done
	mov al, 0x01
	out 0x21, al
	out 0xA1, al

	; Mask IRQs
	in al, 0x21
	mov al, 0xF9
	out 0x21, al

	; Set up IDT at 0x00010000
	mov rdi, 0x00010000

	; IRQs 0-31 are exceptions
	; IRQs 32-255 are interrupts
create_default_isrs:

	mov word [rdi], 0x1234
set_default_isr_low: ; DO NOT REMOVE

	mov dword [rdi+2], 0x8E000008
	mov word [rdi+6], 0x1234
set_default_isr_high: ; DO NOT REMOVE

	add rdi, 0x10
	cmp rdi, 0x00011000
	jne create_default_isrs

	; Load IDT
	lidt [IDTR64]



	; Set up kernel

	; Copy the kernel to 0x00011000
	mov rsi, 0x00078400
	mov rdi, 0x00011000
	mov rcx, 0x40
	rep movsq

	; Jump to kernel
	jmp 0x00011000



segment .data
	AHCI_INIT_NO_AHCI db "Could not find AHCI compatible SATA device", 0
	AHCI_INIT_NO_IMPLEMENTED_AHCI_PORTS db "No implemented AHCI ports", 0

IDTR64:					; Interrupt Descriptor Table Register
dw 0x0FFF				; limit of IDT (size minus one) (4096 bytes - 1)
dq 0x0000000000010000	; linear address of IDT



segment .bss
	AHCI_PORT resb 1
	AHCI_BAR5 resd 1
	AHCI_HBA_ADDR resd 1
	AHCI_PORT_ADDR resq 1
	AHCI_COMMAND_LIST_ADDRESS resq 1
	AHCI_COMMAND_TABLE_ADDRESS resq 1
	AHCI_FIS_RECEIVED_ADDRESS resq 1


