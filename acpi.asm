segment .text
	; Check cpuid and load APIC stuff
	xor rax, rax
	xor rcx, rcx
	xor rdx, rdx
	cpuid

	cmp rax, 0
	jne cpuid_supports_acpi
	mov rsi, ACPI_UNSUPPORTED
	jmp error

cpuid_supports_acpi:
	mov rax, 1
	xor rcx, rcx
	xor rdx, rdx

	cpuid

	mov dword [CPU_INFO], eax
	mov dword [CPU_INFO+4], ebx
	mov dword [CPU_INFO+8], ecx
	mov dword [CPU_INFO+12], edx

	and rdx, (1 << 9)
	jnz cpu_supports_acpi

	mov rsi, ACPI_UNSUPPORTED
	jmp error

cpu_supports_acpi:
	; Find RSDP
	mov rbx, 'RSD PTR '
	xor rsi, rsi
	mov esi, 0x000E0000

check_for_rsdp_signature:
	lodsq
	cmp rax, rbx
	je found_rsdp
	add rsi, 8
	cmp rsi, 0x000FFFFF
	jl check_for_rsdp_signature
	mov rsi, NO_RSDT
	jmp error

found_rsdp:
	mov cl, 12
	mov bl, 0x1F

validate_rsdp_checksum:
	lodsb
	add bl, al
	dec cl
	jnz validate_rsdp_checksum
	cmp bl, 0
	je validated_rsdp
	sub rsi, 12
	jmp check_for_rsdp_signature

validated_rsdp:
	sub rsi, 5
	lodsb

	cmp al, 1
	jne read_rsdt
	mov rsi, ACPI_TOO_OLD
	jmp error

read_rsdt:
	add rsi, 4

	mov cl, 16
	mov bl, 0

validate_rsdt_checksum:
	lodsb
	add bl, al
	dec cl
	jnz validate_rsdt_checksum
	cmp bl, 0
	je read_xsdt
	mov rsi, INVALID_RSDT_CHECKSUM

read_xsdt:
	sub rsi, 12
	lodsq
	mov rsi, rax

	lodsd
	cmp eax, 'XSDT'
	je validate_xsdt
	mov rsi, INVALID_XSDT_CHECKSUM
	jmp error

validate_xsdt:
	lodsd
	mov ecx, eax
	mov edx, ecx
	mov bl, 0x43
	sub rsi, 4
	sub ecx, 4

validate_xsdt_checksum:
	lodsb
	add bl, al
	dec ecx
	jnz validate_xsdt_checksum
	cmp bl, 0
	je valid_xsdt_checksum
	mov rsi, INVALID_XSDT_CHECKSUM
	jmp error

valid_xsdt_checksum:
	mov rcx, rsi
	sub edx, 36
	sub rsi, rdx

read_xsdt_tables:
	lodsq
	mov r14, rsi
	mov rsi, rax
	lodsd
	cmp eax, 'APIC'
	je found_madt
	mov rsi, r14
	cmp rcx, rsi
	jg read_xsdt_tables
	mov rsi, NO_MADT
	jmp error

found_madt:
	lodsd
	mov ecx, eax
	mov edx, ecx
	sub rsi, 4
	sub ecx, 4
	mov bl, 0x1D

validate_madt_checksum:
	lodsb
	add bl, al
	dec ecx
	jnz validate_madt_checksum
	cmp bl, 0
	je valid_madt_checksum
	mov rsi, INVALID_MADT_CHECKSUM
	jmp error

valid_madt_checksum:
	sub edx, 36
	mov ecx, edx
	sub rsi, rcx

	lodsd
	mov [LOCAL_APIC], eax
	add rsi, 4
	sub ecx, 8

read_madt:
	cmp ecx, 0
	je done_madt
	xor rax, rax
	lodsb
	mov dl, al
	lodsb
	sub ecx, eax
	cmp dl, 1
	jl read_madt_type0
	je read_madt_type1
	cmp dl, 3
	jl read_madt_type2
	je read_madt_type4
	jg read_madt_type5

read_madt_type0:
	add rsi, 2
	lodsd
	and eax, 1
	jz read_madt

	inc byte [CPU_COUNT]
	jmp read_madt

read_madt_type1:
	add rsi, 10
	jmp read_madt

read_madt_type2:
	add rsi, 8
	jmp read_madt

read_madt_type4:
	mov rsi, ADD_MADT4
	jmp error

read_madt_type5:
	mov rsi, ADD_MADT5
	jmp error

done_madt:
	mov dword [LOCAL_APIC + 0x80], 0
	mov dword [LOCAL_APIC + 0xD0], 0x01000000
	mov dword [LOCAL_APIC + 0xE0], 0xFFFFFFFF
	mov dword [LOCAL_APIC + 0xF0], 0x000001F0

segment .data
	DONE db "Done so far",0
	ACPI_TOO_OLD db "ACPI v1.0 not supported", 0
	ACPI_UNSUPPORTED db "CPU does not support ACPI", 0
	INVALID_MADT_CHECKSUM db "Invalid MADT Checksum", 0
	INVALID_RSDT_CHECKSUM db "Invalid RSDT Checksum", 0
	INVALID_XSDT_CHECKSUM db "Invalid XSDT Checksum", 0
	NO_MADT db "Unable to locate MADT", 0
	NO_RSDT db "Unable to locate RSDT", 0

	ADD_MADT4 db "Hey, add support for MADT4", 0
	ADD_MADT5 db "Hey, add support for MADT5", 0

	CPU_COUNT db 0

segment .bss
	CPU_INFO resb 16
	LOCAL_APIC resb 4
