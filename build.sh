#!/bin/bash

# Assmeble the final stage bootloader
nasm init.asm -f bin -o kernel.img -w+orphan-labels
PAD=$((512 - (`stat -c%s kernel.img` % 512)))
dd if=/dev/zero bs=$PAD count=1 >> kernel.img


# Now assemble and add kernel
nasm main.asm -f bin -o main.img -w+orphan-labels
PAD=$((512 - (`stat -c%s main.img` % 512)))
dd if=main.img >> kernel.img
dd if=/dev/zero bs=$PAD count=1 >> kernel.img
rm main.img

# Do some "linking"
ADDR="0x`grep set_default_isr_high map-kernel-init.map | sed 's/  */ /g' | cut -d\  -f3`"
ORIGIN="0x`head -n9 map-kernel-init.map | tail -1`"
HOFFSET="`printf '%d' $(($ADDR - $ORIGIN - 2))`"

ADDR="0x`grep set_default_isr_low map-kernel-init.map | sed 's/  */ /g' | cut -d\  -f3`"
ORIGIN="0x`head -n9 map-kernel-init.map | tail -1`"
LOFFSET="`printf '%d' $(($ADDR - $ORIGIN - 2))`"

ISR="00000000`grep default_isr map-kernel-main.map | sed 's/  */ /g' | cut -d\  -f3`"
ISR="${ISR: -8}"



# low bytes
HEX="${ISR: -2}";
ISR="${ISR:0:-2}";

echo "`printf '%X' $LOFFSET`: $HEX" | xxd -r - kernel.img

((LOFFSET++));

HEX="${ISR: -2}";
ISR="${ISR:0:-2}";

echo "`printf '%X' $LOFFSET`: $HEX" | xxd -r - kernel.img



# high bytes
HEX="${ISR: -2}";
ISR="${ISR:0:-2}";

echo "`printf '%X' $HOFFSET`: $HEX" | xxd -r - kernel.img

((HOFFSET++));

HEX="${ISR: -2}";
ISR="${ISR:0:-2}";

echo "`printf '%X' $HOFFSET`: $HEX" | xxd -r - kernel.img
