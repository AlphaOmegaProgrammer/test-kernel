segment .text

keyboard_handler:
	cli

	push ax

	in al, 0x60
	mov byte [KEY], al
	and byte [KEY], 0x7F
	cmp byte [KEY], 0x45
	je keyboard_handler_done

	shr al, 7
	mov [KEY_UP], al
	inc al

	mov byte [EVENT], 0x21

keyboard_handler_done:
	mov al, 0x20
	out 0x20, al

	pop ax

	iretq

segment .bss
	KEY resb 1
	KEY_UP resb 1
