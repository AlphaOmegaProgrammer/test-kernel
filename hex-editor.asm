segment .text

hex_editor_interrupt:
	mov al, [EVENT]
	cmp al, 0x21
	jne hex_editor_main_done

	mov al, [KEY_UP]
	cmp al, 1
	je hex_editor_main_done

	mov al, [HEX_EDITOR_STATE]
	mov ah, [KEY]

	; If h is pressed and we're not running, start
	cmp ax, 0x2300
	je hex_editor_start

hex_editor_started:
	; If escape is pressed, stop
	cmp ah, 0x01
	je hex_editor_stop

hex_editor_stopped:
	; If hex editor isn't running, do nothing
	mov al, [HEX_EDITOR_STATE]
	cmp al, 0
	je hex_editor_main_done


	; Check if [0-9a-f] was pressed
	cmp byte [KEY], 0x0B
	je hex_editor_0

	cmp byte [KEY], 0x02
	je hex_editor_1

	cmp byte [KEY], 0x03
	je hex_editor_2

	cmp byte [KEY], 0x04
	je hex_editor_3

	cmp byte [KEY], 0x05
	je hex_editor_4

	cmp byte [KEY], 0x06
	je hex_editor_5

	cmp byte [KEY], 0x07
	je hex_editor_6

	cmp byte [KEY], 0x08
	je hex_editor_7

	cmp byte [KEY], 0x09
	je hex_editor_8

	cmp byte [KEY], 0x0A
	je hex_editor_9

	cmp byte [KEY], 0x1E
	je hex_editor_a

	cmp byte [KEY], 0x30
	je hex_editor_b

	cmp byte [KEY], 0x2E
	je hex_editor_c

	cmp byte [KEY], 0x20
	je hex_editor_d

	cmp byte [KEY], 0x12
	je hex_editor_e

	cmp byte [KEY], 0x21
	je hex_editor_f


	; Page up and down
	cmp byte [KEY], 0x49
	je hex_editor_page_up

	cmp byte [KEY], 0x51
	je hex_editor_page_down


	; w (write to disk)
	cmp byte [KEY], 0x11
	je hex_editor_write



	; Check if arrow keys were pressed
	cmp byte [KEY], 0x4D
	je hex_editor_right_arrow

	cmp byte [KEY], 0x4B
	je hex_editor_left_arrow

	cmp byte [KEY], 0x48
	je hex_editor_up_arrow

	cmp byte [KEY], 0x50
	je hex_editor_down_arrow

hex_editor_main_done:
	ret



; [0-9a-f] handlers
hex_editor_0:
	mov rax, 0x00
	jmp hex_editor_modify_memory

hex_editor_1:
	mov rax, 0x01
	jmp hex_editor_modify_memory

hex_editor_2:
	mov rax, 0x02
	jmp hex_editor_modify_memory

hex_editor_3:
	mov rax, 0x03
	jmp hex_editor_modify_memory

hex_editor_4:
	mov rax, 0x04
	jmp hex_editor_modify_memory

hex_editor_5:
	mov rax, 0x05
	jmp hex_editor_modify_memory

hex_editor_6:
	mov rax, 0x06
	jmp hex_editor_modify_memory

hex_editor_7:
	mov rax, 0x07
	jmp hex_editor_modify_memory

hex_editor_8:
	mov rax, 0x08
	jmp hex_editor_modify_memory

hex_editor_9:
	mov rax, 0x09
	jmp hex_editor_modify_memory

hex_editor_a:
	mov rax, 0x0A
	jmp hex_editor_modify_memory

hex_editor_b:
	mov rax, 0x0B
	jmp hex_editor_modify_memory

hex_editor_c:
	mov rax, 0x0C
	jmp hex_editor_modify_memory

hex_editor_d:
	mov rax, 0x0D
	jmp hex_editor_modify_memory

hex_editor_e:
	mov rax, 0x0E
	jmp hex_editor_modify_memory

hex_editor_f:
	mov rax, 0x0F

hex_editor_modify_memory:
	mov rbx, 0xF0
	xor rdi, rdi
	mov di, word [HEX_EDITOR_NIBBLE_COUNTER]
	shr rdi, 1
	shl rdi, 1
	cmp di, word [HEX_EDITOR_NIBBLE_COUNTER]
	jne hex_editor_modify_memory_odd
	shl rax, 4
	shr rbx, 4

hex_editor_modify_memory_odd:
	shr rdi, 1
	add rdi, [HEX_EDITOR_BUFFER_ADDR]

	mov dl, [HEX_EDITOR_BUFFER_PAGE]
	cmp dl, 0
	je hex_editor_modify_memory_paged

	add rdi, 0x100

hex_editor_modify_memory_paged:

	and [rdi], bl
	add [rdi], al

	call hex_editor_print_buffer

	jmp hex_editor_right_arrow



; Page up/down (switch between low and high 256 bytes of sector)
hex_editor_page_up:
	mov byte [HEX_EDITOR_BUFFER_PAGE], 0
	call hex_editor_print_buffer

	ret

hex_editor_page_down:
	mov byte [HEX_EDITOR_BUFFER_PAGE], 1
	call hex_editor_print_buffer

	ret



; Write sectors to hard drive
hex_editor_write:
	call clear_regs

	mov rdx, 1
	mov cx, 1

	mov r8, [HEX_EDITOR_BUFFER_ADDR]
	xchg r8, [AHCI_WRITE_BUFFER_ADDR]

	call ahci_dma_write
	call ahci_dma_wait

	xchg r8, [AHCI_WRITE_BUFFER_ADDR]

	ret



; Arrow key handlers
hex_editor_up_arrow:
	cmp word [HEX_EDITOR_NIBBLE_COUNTER], 0x20
	jl hex_editor_main_done
	sub word [HEX_EDITOR_NIBBLE_COUNTER], 0x20
	jmp hex_editor_move_cursor

hex_editor_down_arrow:
	cmp word [HEX_EDITOR_NIBBLE_COUNTER], 0x1E0
	jge hex_editor_main_done
	add word [HEX_EDITOR_NIBBLE_COUNTER], 0x20
	jmp hex_editor_move_cursor

hex_editor_left_arrow:
	cmp word [HEX_EDITOR_NIBBLE_COUNTER], 0
	je hex_editor_main_done
	sub word [HEX_EDITOR_NIBBLE_COUNTER], 1
	jmp hex_editor_move_cursor

hex_editor_right_arrow:
	cmp word [HEX_EDITOR_NIBBLE_COUNTER], 0x01FF
	je hex_editor_main_done
	add word [HEX_EDITOR_NIBBLE_COUNTER], 1
	jmp hex_editor_move_cursor



; Hex editor functions
hex_editor_start:
	mov byte [HEX_EDITOR_STATE], 1
	mov byte [HEX_EDITOR_BUFFER_PAGE], 0

	call clear_screen

	call clear_regs
	mov rsi, HEX_EDITOR_TEXT1
	mov rax, 0x11
	call print_string

	call clear_regs
	mov rsi, HEX_EDITOR_TEXT2
	mov rax, 0x12
	call print_string

	call clear_regs
	mov rsi, HEX_EDITOR_TEXT3
	mov rax, 0x14
	call print_string

	call clear_regs
	mov rsi, HEX_EDITOR_TEXT4
	mov rax, 0x15
	call print_string


	mov byte [0x000B810C], 0x4C
	mov byte [0x000B810E], 0x42
	mov byte [0x000B8110], 0x41

	mov byte [0x000B8114], 0x30
	mov byte [0x000B8116], 0x78
	mov byte [0x000B8118], 0x30
	mov byte [0x000B811A], 0x31


	mov r8, [HEX_EDITOR_BUFFER_ADDR]
	xchg r8, [AHCI_READ_BUFFER_ADDR]

	mov rdx, [HEX_EDITOR_EDIT_LBA]
	mov rcx, 1

	call ahci_dma_read
	call ahci_dma_wait

	mov [AHCI_READ_BUFFER_ADDR], r8

	call hex_editor_print_buffer

	mov rsi, [HEX_EDITOR_CURSOR_ADDR]
	mov byte [rsi], 0x0A

	jmp hex_editor_started

hex_editor_stop:
	mov byte [HEX_EDITOR_STATE], 0

	call clear_screen
	call clear_regs
	mov rsi, WELCOME
	call print_string

	jmp hex_editor_stopped

hex_editor_move_cursor:
	; Clear previous nibble color
	mov rdi, qword [HEX_EDITOR_CURSOR_ADDR]
	mov byte [rdi], 0x0F

	; Figure out the nibble to highlight
	xor rax, rax
	xor rdi, rdi
	mov ax, [HEX_EDITOR_NIBBLE_COUNTER]
	mov r8, rax
	shr rax, 5
	mov rbx, 0x1C
	mul rbx
	mov rdi, rax
	mov rax, r8
	shr ax, 3
	add rdi, rax
	mov rax, r8
	shr ax, 1
	add rdi, rax
	add rdi, r8
	shl rdi, 1

	add rdi, 0x000B8001
	mov [HEX_EDITOR_CURSOR_ADDR], rdi
	mov byte [rdi], 0x0A

	jmp hex_editor_main_done



hex_editor_print_buffer:
	call clear_regs
	mov al, [HEX_EDITOR_BUFFER_PAGE]
	mov rcx, 0x100
	mul rcx

	mov rsi, rax

	xor rax, rax
	xor rdx, rdx

	add rsi, [HEX_EDITOR_BUFFER_ADDR]
	mov rcx, 0x100

	call print_hex

	ret



; HEX_EDITOR_STATE
; 0x00 = Stopped
; 0x01 = Editing sector
; 0x02 = Reading LBA

segment .data
	HEX_EDITOR_STATE db 0
	HEX_EDITOR_EDIT_LBA dq 1
	HEX_EDITOR_BUFFER_ADDR dq 0x0000000000074000
	HEX_EDITOR_BUFFER_PAGE db 0

	HEX_EDITOR_NIBBLE_COUNTER dw 0
	HEX_EDITOR_CURSOR_ADDR dq 0x00000000000B8001

	HEX_EDITOR_TEXT1 db "r = read sector from disk", 0
	HEX_EDITOR_TEXT2 db "w = write data to disk", 0
	HEX_EDITOR_TEXT3 db "pg up = Bytes 0 to 255 of sector", 0
	HEX_EDITOR_TEXT4 db "pg dn = Bytes 256 to 511 of sector", 0
