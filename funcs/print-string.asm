; rsi: Memory address to start of string
; rax: Line number
; rbx: Column number

segment .text

print_string:
	xor rdi, rdi
	mov rdi, rax
	imul rdi, 160
	add rdi, 0x000B8000
	shl rbx, 1
	add rdi, rbx

print_string_char:
	lodsb
	mov byte [rdi], al
	add rdi, 2
	cmp al, 0
	jne print_string_char

	ret
