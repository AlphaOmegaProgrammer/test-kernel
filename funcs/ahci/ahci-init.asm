segment .text
ahci_init:
	mov eax, 0x80000008
	mov ebx, eax

ahci_init_find_ahci:
	mov dx, 0x0CF8
	out dx, eax
	mov dx, 0x0CFC
	in eax, dx

	shr eax, 8
	cmp eax, 0x00010601
	je ahci_init_found_ahci

	add ebx, 0x00000100
	mov eax, ebx
	cmp ebx, 0x81000000
	jl ahci_init_find_ahci

	mov rsi, AHCI_INIT_NO_AHCI
	jmp error

; Putting some stuff for searching for AHCI ports here because jumping is messy
ahci_init_no_implemented_ahci_ports:
	mov rsi, AHCI_INIT_NO_IMPLEMENTED_AHCI_PORTS
	jmp error

ahci_init_check_next_ahci_port:
	inc byte [AHCI_PORT]
	add esi, 0x80
	shr eax, 1
	jmp ahci_init_find_implemented_ahci_port

ahci_init_found_ahci:
	sub ebx, 8
	mov [AHCI_HBA_ADDR], ebx
	mov eax, ebx
	add eax, 0x24

	mov edx, 0x0CF8
	out dx, eax
	mov edx, 0x0CFC
	in eax, dx
	mov [AHCI_BAR5], eax

	; AHCI Specification Chapter 10.1.2
	xor rsi, rsi
	mov esi, eax

	; Step 1: Set GHC.AE to 1 (Activate AHCI Enable)
	mov eax, [rsi + 0x04]
	or eax, (1 << 31)
	mov [rsi + 0x04], eax

	;Step 2: Search for implemented ports
	mov eax, [rsi + 0x0C]
	cmp eax, 0
	je ahci_init_no_implemented_ahci_ports

	add esi, 0x100
ahci_init_find_implemented_ahci_port:
	cmp eax, 0
	je ahci_init_no_implemented_ahci_ports

	mov ebx, eax
	or ebx, 1
	cmp ebx, eax
	jne ahci_init_check_next_ahci_port

	cmp dword [rsi + 0x28], 0
	je ahci_init_check_next_ahci_port

	; Now we've found an implemented port address
	mov [AHCI_PORT_ADDR], rsi

	; Step 3: Ensure idle state
	mov eax, [rsi + 0x18]
	mov ebx, eax
	and eax, 0x0000C011
	jz ahci_init_ahci_idle

	mov eax, ebx
	and eax, 0xFFFFFFFE
	mov [rsi + 0x20], eax

	; If not already idle, clear FRE and CR (Bits 4 and 15 respectively)
ahci_init_ahci_clear_cr:
	mov eax, [rsi + 0x20]
	mov ebx, eax
	or ebx, (1 << 15)
	jnz ahci_init_ahci_clear_fre
	and eax, 0xFFFF7FFF
	mov [rsi + 0x20], eax
	jmp ahci_init_ahci_clear_cr

ahci_init_ahci_clear_fre:
	mov eax, [rsi + 0x20]
	mov ebx, eax
	or ebx, (1 << 4)
	jnz ahci_init_ahci_idle
	and eax, 0xFFFFFFEF
	mov [rsi + 0x20], eax
	jmp ahci_init_ahci_clear_fre

ahci_init_ahci_idle:

	; Step 4: Read number of command slots (I don't see how this is immediately relevant, so I'm going to skip this for now)

	; Step 5: Set PxCLB and PxCLBU to the physical address of the allocated command list
	; It should be noted that Command List must be located at 1K aligned memory address and Received FIS be 256 bytes aligned.
	xor rax, rax
	mov rax, 0x00070000
	mov qword [AHCI_COMMAND_LIST_ADDRESS], rax
	mov qword [rsi + 0x00], rax

	; Set PxFB and PxFBU to the physical address of the allocated FIS receive area
	mov rax, 0x00071000
	mov qword [AHCI_FIS_RECEIVED_ADDRESS], rax
	mov qword [rsi + 0x08], rax

	mov rax, 0x00072000
	mov qword [AHCI_COMMAND_TABLE_ADDRESS], rax

	; I should probably zero the allocated memory here

	; Set PxCMD.FRE (Bit 4)
	mov eax, [rsi + 0x18]
	or eax, (1 << 4)
	mov [rsi + 0x18], eax

	; Step 6: Clear the PxSERR register, by writing ‘1s’ to each implemented bit location
	mov dword [rsi + 0x30], 0x07FF0F03

	; Step 7: Set up Interrupts (Going to disable all of them for the time being, so do nothing)

	ret

segment .data
	AHCI_PORT db 0

	AHCI_INIT_NO_AHCI db "Could not find AHCI compatible SATA device", 0
	AHCI_INIT_NO_IMPLEMENTED_AHCI_PORTS db "No implemented AHCI ports", 0

segment .bss
	AHCI_BAR5 resd 1
	AHCI_HBA_ADDR resd 1
	AHCI_PORT_ADDR resq 1
	AHCI_COMMAND_LIST_ADDRESS resq 1
	AHCI_COMMAND_TABLE_ADDRESS resq 1
	AHCI_FIS_RECEIVED_ADDRESS resq 1
