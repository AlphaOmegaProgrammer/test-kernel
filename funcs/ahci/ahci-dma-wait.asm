segment .text

ahci_dma_wait:
	mov rsi, [AHCI_FIS_RECEIVED_ADDRESS]

ahci_dma_wait_loop:
	mov al, [rsi + 0x40]
	cmp al, 0
	je ahci_dma_wait_loop

;	mov rsi, [AHCI_PORT_ADDR]
;	mov eax, [rsi + 0x18]
;	and eax, 0xFFFFFFEE
;	mov [rsi + 0x18], eax

ahci_dma_write_ret:
	ret
