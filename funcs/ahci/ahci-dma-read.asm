; rdx = LBA
; cx = Number of sectors

segment .text

; No interrupts yet! Call ahci_dma_wait
ahci_dma_read:
	push cx

	mov eax, dword [AHCI_PORT+0x18]
	or eax, (1 << 28)
	jnz ahci_dma_read_continue

	mov esi, AHCI_READ_PORT_NOT_ACTIVE
	jmp error

ahci_dma_read_continue:
	; Clear FIS received memory (needed for ahci_dma_wait)
	xor rax, rax
	mov rdi, [AHCI_FIS_RECEIVED_ADDRESS]
	mov rcx, 32
	rep stosq

	; Set up command list
	mov rsi, [AHCI_COMMAND_LIST_ADDRESS]
	xor rax, rax
	mov rax, 0x00010005
	mov qword [rsi], rax
	mov dword [rsi + 16], 0
	mov dword [rsi + 20], 0
	mov dword [rsi + 24], 0

	; Get command table address (We still need to add this to the command list)
	mov rdi, [AHCI_COMMAND_TABLE_ADDRESS]
	mov [rsi + 8], rdi

	; Set up command table (Issue ATAPI command 0x25 to read the sectors)
	mov rax, rdx
	and eax, 0x00FFFFFF
	shl rax, 32
	add rax, 0x00258027
	mov [rdi], rax

	mov rax, rdx
	shr rax, 24
	and eax, 0x00FFFFFF
	pop cx
	shl rcx, 32
	add rax, rcx

	mov [rdi + 8], rax
	mov dword [rdi + 16], 0

	mov rax, [AHCI_READ_BUFFER_ADDR]
	mov qword [rdi + 0x80], rax

	xor rax, rax
	mov rax, 0xFF
	shl rax, 32
	mov qword [rdi + 0x88], rax

	mov rsi, [AHCI_PORT_ADDR]
	mov dword [rsi + 0x10], 0
	mov dword [rsi + 0x38], 1

	mov dword [rsi + 0x18], 0x11

	ret


segment .data
	AHCI_READ_PORT_NOT_ACTIVE db "AHCI Read failed: Port inactive", 0
	AHCI_READ_BUFFER_ADDR dq 0x0000000000073000
