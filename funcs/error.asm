segment .text

error:
	xor rax, rax
	call print_string

error_loop:
	hlt
	jmp error_loop
