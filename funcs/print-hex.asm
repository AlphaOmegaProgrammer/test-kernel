; rsi: Memory address to start from
; rax: Line number
; rcx: Total number of bytes to print (256 is a good idea)

print_hex:
	xor rdi, rdi
	mov rdi, rax
	imul rdi, 0xA0
	add rdi, 0x000B8000

	mov byte [PRINT_HEX_CHARS_IN_COL], 0
	mov byte [PRINT_HEX_COLS_IN_LINE], 0
	mov byte [PRINT_HEX_LINE], al

print_hex_byte:
	lodsb
	mov ah, al
	shr al, 4
	and ah, 0x0F

	cmp al, 10
	jl print_hex_byte_1
	add al, 7

print_hex_byte_1:
	cmp ah, 10
	jl print_hex_byte_2
	add ah, 7

print_hex_byte_2:
	add al, 48
	add ah, 48

	mov [rdi], al
	add rdi, 2
	mov al, ah
	mov [rdi], al
	add rdi, 2
	mov byte [rdi], 0x20
	add rdi, 2

	dec ecx
	jz print_hex_done

	inc byte [PRINT_HEX_CHARS_IN_COL]
	cmp byte [PRINT_HEX_CHARS_IN_COL], 4
	jne print_hex_byte

	mov byte [PRINT_HEX_CHARS_IN_COL], 0
	mov byte [rdi], 0x20
	add rdi, 2

	inc byte [PRINT_HEX_COLS_IN_LINE]
	cmp byte [PRINT_HEX_COLS_IN_LINE], 4
	jne print_hex_byte

	mov byte [PRINT_HEX_COLS_IN_LINE], 0
	inc byte [PRINT_HEX_LINE]
	mov di, [PRINT_HEX_LINE]
	and rdi, 0x000000FF
	imul rdi, 0xA0
	add rdi, 0x000B8000
	jmp print_hex_byte

print_hex_done:
	ret



segment .bss
	PRINT_HEX_CHARS_IN_COL resb 1
	PRINT_HEX_COLS_IN_LINE resb 1
	PRINT_HEX_LINE resb 1
